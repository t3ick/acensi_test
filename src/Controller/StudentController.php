<?php

namespace App\Controller;

use App\Entity\Student;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;


class StudentController extends AbstractController
{
    /**
     * @Route("/", name="app_student")
     */
    public function index(EntityManagerInterface $em, Request $request): Response
    {
        $student = new Student();

        $form = $this->createFormBuilder($student)
            ->add('firstname', TextType::class)
            ->add('lastname', TextType::class)
            ->add('numEtud', NumberType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Student'])
            ->getForm();

        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em->persist($student);
            $em->flush();
        }

        $repo = $this->getDoctrine()->getRepository(Student::class);
        $aStudent = $repo
            ->findBy([]);
        ;

        return $this->render('student/index.html.twig', [
            'form' => $form->createView(),
            'aStudent' => $aStudent,
        ]);
    }
}
